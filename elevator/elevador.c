/*
   Programa elevador para motivos didacticos
   Copyright (C) 2018 energy1011[at]gmail[dot]com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
  */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define MAX_PISOS 10
#define MAX_PERSONAS 20

int main(){
  // controles del programa
  int opcion_usuario, salida = 0;

  //  elevador y sus controles
  int piso_actual = 0;  // Inicia en planta baja
  int direccion = 0;
  int piso_a_ir;
  int personas_que_suben;

  // contadores 
  int total_elevador_ha_subido = 0;
  int total_elevador_ha_bajado = 0;
  int total_personas_han_subido= 0;
  int total_personas_han_bajado = 0;
  int total_pisos_recorridos = 0;
  int total_pisos_recorridos_abajo = 0;
  int total_pisos_recorridos_arriba = 0;
  int total_personas_transportadas = 0;

  printf("Bienvenido a este elevador")
    do{
      // Inicializo la direccion
      direccion = 0;
      printf("\nSeleccione una opcion:\n1-Subir\n2-Bajar\n3-Resumen\n4-Salir\nPiso Actual [%i]\nOpcion:",piso_actual);
      scanf("%i",&opcion_usuario);

      switch(opcion_usuario){
        // subir
        case 1: 
          direccion = 1;
          break;
          // bajar  
        case 2: 
          direccion = 2;
          break;
          // resumen
        case 3:
          printf("\n== Resumen del elevador ==");
          printf("\n\tCuantas veces a subido: %i",total_elevador_ha_subido);
          printf("\n\tCuantas veces a bajado: %i",total_elevador_ha_bajado);
          printf("\n\tCuantas personas han subido: %i",total_personas_han_subido);
          printf("\n\tCuantas personas han bajado: %i",total_personas_han_bajado);
          printf("\n\tTotal pisos recorridos: %i",total_pisos_recorridos);
          printf("\n\tTotal pisos recorridos hacia arriba: %i",total_pisos_recorridos_arriba);
          printf("\n\tTotal pisos recorridos hacia abajo: %i",total_pisos_recorridos_abajo);
          continue;
          break;
          // salida
        case 4:
          printf("\nBye.");
          salida = 1;
          continue;
          break;
          break;
        default:
          printf("\nOpcion no encontrada, no vamos para ningun lado");
          continue;
          break;
      } // fin switch

      printf("\nA que piso va ?");
      scanf("%i",&piso_a_ir);
      if( piso_a_ir > MAX_PISOS || piso_a_ir < 0 ) {
        printf("Ese piso no existe.");
        printf("\nEste elevador puede llevarlo del piso cero (PB) al piso %i como max.\n",MAX_PISOS);
        continue;

      }
      if(piso_a_ir == piso_actual) {
        printf("\nYa nos encontramos en el piso: %i",piso_actual);
        continue;
      }

      // Compruebo que no se quiera subir a un piso con numero menor y vice versa
      if(direccion == 1){
        if(piso_a_ir < piso_actual){
          printf("\nNo se puede SUBIR al piso [%i] ya que nos encontramos en el piso [%i]\n",piso_a_ir, piso_actual);
          printf("\nQuizo decir BAJAR, entonces bajaremos..\n");
          direccion = 2;
        }
      }else{
        if(piso_a_ir > piso_actual){
          printf("\nNo se puede BAJAR al piso[%i] ya que nos encontramos en el piso [%i]\n",piso_a_ir, piso_actual);
          printf("\nQuizo decir SUBIR, entonces subiremos..\n");
          direccion = 1;
        }
      }

      printf("\nCuantas personas ?");
      scanf("%i",&personas_que_suben);
      if( personas_que_suben > MAX_PISOS) {
        printf("\n¡¡ Peligro !!, no nos moveremos, elevador con capacidad MAX de %i\n",MAX_PERSONAS);
        continue;
      }

      if(personas_que_suben <= 0){
        char *str;
        printf("\n ¿%i Personas ?, No puedo subir a esa cantidad de gente, ya que soy un elevador inteligente que lleva un resumen con conteo de personas, si desea abrir las puertas y bajar del elevador presione el boton de abrir puertas, En caso de emergencia presione el boton de alerta.", personas_que_suben);
        sprintf(str,"espeak");
        system(str);
      }

      // Vamos para arriba ?
      if(direccion == 1){
        printf("\nVamos para arriba..");
        total_elevador_ha_subido++;
        total_personas_han_subido += personas_que_suben;
        total_pisos_recorridos += ((piso_actual - piso_a_ir) * -1);
        total_pisos_recorridos_arriba += (piso_a_ir - piso_actual);
      }else{
        printf("\nVamos para abajo...");
        total_elevador_ha_bajado++;
        total_personas_han_bajado += personas_que_suben;
        total_pisos_recorridos += (piso_actual - piso_a_ir);
        total_pisos_recorridos_abajo += (piso_actual - piso_a_ir);
      }
      // Muevo el "elevador"
      int piso = piso_actual;
      printf("\nPiso Actual ");
      do{
        if(direccion == 1){
          piso++;
        }else{
          piso--;
        }
        printf("\b [%i]\b\b\b",piso);
        fflush(stdout);
        sleep(1);
      }while(piso != piso_a_ir);
      printf("\nHemos llegado, cuidado al bajar.\n");
      piso_actual = piso_a_ir;
    }while(!salida);
  return 0;
}
