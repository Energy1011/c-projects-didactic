/* 
 Programa que muestra como escribir una funcion custom para errores llamada 'error' que imprime el numero de linea, nombre de archivo y el mensaje de error.
 Códigos de error del sistema:
 $less /usr/include/asm-generic/errno-base.h
*/
#include <stdio.h>
#include <string.h>
#include <errno.h>

// Prototipo de funciones
void error(int line, char *file, const char *msg);

// Funcion custom para mostrar un mensaje de error con mumero de linea y nombre de archivo
void error(int line, char *file, const char *msg){
  fprintf(stderr, "(%s) %i %s\n", file, line, strerror(errno)); 
}

int main () {
   FILE *fp;
   fp = fopen("file.txt","r");
   if( fp == NULL ) {
     // LLamada a nuestra funcion custom para los mensajes de error
      error(__LINE__, __FILE__, "Error al abrir el archivo");
   }
   return(0);
}
