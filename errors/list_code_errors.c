/* 
 Programa que muestra el listado de códigos de error del sistema operativo
 Escribir un programa que presente en el fichero estandar de salida una lista con todo los códigos de error que manejan las llamadas al sistema UNIX.
 Este programa se podia escribir con un ciclo for y usando sys_errorlist y sys_nerr.
  El Array sys_errlist ahora es absoleto al igual que sys_nerr, entonces he utilizado el comando perror desde la shell para que este programa muestre el listado.
*/
#include<stdio.h>
#include <stdlib.h>

int main(){
  int i;
  char *str;
  for(i=0; i < 193; i++){
    sprintf(str,"perror %i | grep OS",i);
    // Llamanda al comando shell: perror 
    system(str);
  }
  return 0;
}
