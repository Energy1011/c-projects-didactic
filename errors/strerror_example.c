/* 
   Programa que muestra el uso de la funcion strerror()
   Podemos ver las definiciones de los códigos de error del sistema en:
   $less /usr/include/asm-generic/errno-base.h
*/
#include <stdio.h>
#include <string.h>
#include <errno.h>

int main () {
   FILE *fp;
   fp = fopen("file.txt","r");
   if( fp == NULL ) {
      printf("Error %i: %s\n", errno, strerror(errno));
   }
   return(0);
}
