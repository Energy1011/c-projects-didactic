// Programa que copia el contenido de un fichero a otro con fread and fwrite
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){

  // ptr to file
  FILE *forigen, *fdestino;
  unsigned char c;

   // Compruebo la cantidad de argumentos en la llamada de este programa
   if(argc != 3){
     fprintf(stderr, "Forma de uso: %s <archivo_origen> <archivo_destino>\n", argv[0]);
     exit(-1);
   }

  // Intento abrir el primer archivo en modo lectura
  if( (forigen = fopen(argv[1],"r")) == NULL){
      perror(argv[1]);
      exit(-1);
  }
 
  // Intento crear el archivo si no existe y se abre modo escritura
  if( (fdestino = fopen(argv[2],"w")) == NULL){
      perror(argv[2]);
      exit(-1);
  }

  // Copia de contenido del fichero a otro fichero
  while( (fread(&c, sizeof(c), 1, forigen)) > 0){
    fwrite(&c, sizeof(c), 1, fdestino);
  }

  // Cierre de ficheros
  fclose(forigen);
  fclose(fdestino);
  exit(0);
}
