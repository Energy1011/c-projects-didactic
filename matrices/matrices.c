// Programa con fines didacticos, pide al usuario ingresar dos matrices y las multiplica si son del mismo orden y muestra el resultado
#include <stdio.h>
#include <stdlib.h>

// Definicion de la estructura de una matriz
struct matriz{
  int filas, columnas;
  float **coef;
};

// Prototipos de las funciones
float **reservar_coeficientes(int filas, int columnas);
struct matriz leer_matriz();
struct matriz multiplicar_matrices(struct matriz a, struct matriz b);
void imprimir_matriz(struct matriz m);

// Reserva memoria para los coeficientes de una matriz
float **reservar_coeficientes(int filas, int columnas){
  float **coef;
  int i;
  // Reservo en coef el tamaño de un (float **) por el numero de filas
  if(( coef = (float **)malloc(sizeof(float *) * filas)) == NULL )
    printf("Error al reservar memoria para los coeficientes");
  // Para cada fila
  for( i=0; i < filas; i++){
  // Reservo en coef el tamaño de un (float *) por el numero columnas
    if((coef[i]=(float *)malloc(sizeof(float*) * columnas)) == NULL)
      printf("Error al reservar memoria para los coeficientes");
  }
  return (coef);
}

// Estructura de una matriz
struct matriz leer_matriz(){
  struct matriz m;
  int fila, columna;
  printf("Leyendo una matriz...\n");
  printf("Ingrese las filas de la matriz:\n");
  scanf("%d",&m.filas);
  printf("Ingrese las columnas de la matriz:\n");
  scanf("%d",&m.columnas);
  printf("Reservando los coeficientes de la matriz..\n");
  m.coef = reservar_coeficientes(m.filas, m.columnas);
  printf("Ingrese coeficientes de la matriz");
  for(fila=0; fila < m.filas; fila++){
    for(columna=0; columna < m.columnas; columna++){
      scanf("%f", &m.coef[fila][columna]);
    }
  }
  return (m);
}

// Multiplica matrices
struct matriz multiplicar_matrices(struct matriz a, struct matriz b){
  struct matriz r;
  int i,j,k;
  printf("Multiplicando matrices\n");
  if( a.columnas != b.filas){
    printf("No se pueden multiplicar las matrices, son de distinto orden");
    exit(-1);
  }
  r.filas = a.filas;
  r.columnas = b.columnas;
  r.coef = reservar_coeficientes(r.filas, r.columnas);
  // Multiplicacion de matrices
  for(i=0; i < r.filas; i++){
    for(j=0; j < r.columnas; j++){
        r.coef[i][j] = 0;
        for(k=0; k < a.columnas; k++)
          r.coef[i][j] += (a.coef[i][k] * b.coef[k][j]);
    }
  }
  return (r);
}

// Imprime una matriz en pantalla
void imprimir_matriz(struct matriz m){
  int fila, columna;
  printf("Imprimiendo matriz:\n");
  for(fila=0; fila < m.filas; fila++){
    for(columna=0; columna < m.columnas; columna++){
      printf(" %g ", m.coef[fila][columna]);
    }
    printf("\n");
  }
}
 // Funcion principal
int main(){
  struct matriz a,b,c;
  // Pide al usuario 2 matrices a y b
  a = leer_matriz();
  b = leer_matriz();
  // Multiplica las matrices a x b
  c = multiplicar_matrices(a, b);
  // Imprime la matriz de resultado de la multiplicación 
  printf("Matriz de resultado:\n");
  imprimir_matriz(c);
  return 0;
}
