/* 
 * Programa que muestra un ejemplo del uso de ncurses para no usar conio.h
  Enlaces de referencia:
    - http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/scanw.html#GETCHCLASS
    - https://stackoverflow.com/questions/8792317/why-cant-i-find-conio-h-on-linux#8792443
  Para distribuciones basadas en Debian se puede instalar la libncurses con el siguiente comando:
   $sudo apt-get install libncurses5-dev libncursesw5-dev
  Distros con rpm:
   $ sudo yum install ncurses-devel ncurses
  Este programa se compila con:
    $gcc ncurses.c -lncurses
  Y se ejecuta:
    $./a.out
*/
#include <ncurses.h>/* ncurses.h includes stdio.h */  
#include <string.h>

int main()
{
  char mesg[]="Enter a string: ";/* message to be appeared on the screen */
  char str[80];
  int row,col;/* to store the number of rows and *
  * the number of colums of the screen */
  initscr();/* start the curses mode */
  getmaxyx(stdscr,row,col);/* get the number of rows and columns */
  mvprintw(row/2,(col-strlen(mesg))/2,"%s",mesg);
  /* print the message at the center of the screen */
  getstr(str);
  mvprintw(LINES - 2, 0, "You Entered: %s", str);
  getch();
  endwin();
  return 0;
}
