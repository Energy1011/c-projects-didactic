// pol.h
// Fichero de cabecera con la definicion de los datos y los prototipos de las funciones de la aplicacion de manipulacion de polinimios.
#if !defined (_POL_H_) 
#define _POL_H_ 

// Definicion de los datos
typedef struct{
  int grado;
  float *coef;
} POLINOMIO;

// Prototipo de las funciones
POLINOMIO crear_polinomio (int grado);
POLINOMIO leer_polinomio (int grado);
void escribir_polinomio (POLINOMIO p); 
POLINOMIO multiplicar_polinomios (POLINOMIO p, POLINOMIO q);
POLINOMIO sumar_polinomios (POLINOMIO p, POLINOMIO q);
POLINOMIO simplificar_polinomio (POLINOMIO p);
#endif
