// Programa con fines didacticos, muestra la estructura de directorios de un programa,
// agredando cabeceras y librerias y los comandos para generar los archivos .o .a

/*
  Comandos utiles:
  Ejemplo: comando para generar archivo objeto .o
  $gcc -c pol_es.c -I ../include/ -o ../lib/pol_es.o
  $gcc -c pol_arit.c -I ../include/ -o ../lib/pol_arit.o

  Ejemplo : comando para generar la libreria .a
  $ar -r ../lib/pol.a ../lib/*.o
  El orden para compilar los modulos de este programa con la libreria .a generada es:
  $gcc main.c -I ../include/ ../lib/pol.a -o ../bin/a.out
  o sin la libreria .a generada:
  $gcc main.c pol_es.c pol_arit.c -I ../include/ -o ../bin/a.out
*/

//pol.c
// Modulo donde reside la funcion principal del programa para la manipulacion de polinomios
#include <stdio.h>
#include "pol.h"

int main(){
  POLINOMIO p,q;
  int grado;
  // Lectura de polinomios
  printf("Grado del primer polinomio: \n");
  scanf("%d", &grado);
  printf("Coeficientes del primer polinomio: \n");
  p = leer_polinomio(grado);
  // Lectura del segundo polinomio
  printf("Grado del segundo polinomio: \n");
  scanf("%d", &grado);
  printf("Coeficientes del segundo polinomio: \n");
  q = leer_polinomio(grado);
  // Operaciones con los polinomios
  // Suma
  printf("Resultado de la suma:\n");
  escribir_polinomio(
      simplificar_polinomio(
        sumar_polinomios(p,q)));
  printf("\n");
  // Multiplicar 
  escribir_polinomio(
      simplificar_polinomio(
        multiplicar_polinomios(p,q)));
  return 0;
}
