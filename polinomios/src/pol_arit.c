//pol_arit.c
// Modulo que contiene las funciones para la manipulacion algebraica de los polinomios.
#include <stdio.h>
#include "pol.h"

// Funcion para multiplicar polinomios
POLINOMIO multiplicar_polinomios (POLINOMIO p, POLINOMIO q){
  POLINOMIO r;
  printf("Multiplicando los polinomios pxq\n");
  return r;
}
 // Funcion para sumar polinomios
POLINOMIO sumar_polinomios (POLINOMIO p, POLINOMIO q){
  POLINOMIO r;  
  printf("Sumando los polinomios p+q\n");
  return r;
}

// Funcion para simplificar un polinomio
POLINOMIO simplificar_polinomio (POLINOMIO p){
  POLINOMIO s;
  printf("Simplificando el polinomio p\n");
  return s;
}
