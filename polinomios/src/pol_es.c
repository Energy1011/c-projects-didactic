// pol_es.h
// Modulo del programa de manipulacion simbolica de polinomios donde se implementan las funciones de entrada/salida
#include <stdio.h>
#include <stdlib.h>
#include "pol.h" 

// funcion de lectura
POLINOMIO leer_polinomio (int grado){
  POLINOMIO p;
  printf("Leyendo el polinomio %i \n", grado);
  return p;
}

// funcion de escritura del polinomio
void escribir_polinomio (POLINOMIO p){
  printf("escribiendo el polinomio\n");
}

// function que reserva memoria para los coeficientes
POLINOMIO crear_polinomio (int grado){
  POLINOMIO p;
  printf("creando polinomio de grado %i \n",grado);
  return p;
}
